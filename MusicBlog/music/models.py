from tkinter import CASCADE

from django.db import models

# Create your models here.

class Album(models.Model):
  GENRE_TYPES = (
    ('EDM', 'Electronic Dance Music'),
    ('Indie Rock', 'Indie Rock'),
    ('NDH', 'Neue Deutsche Härte'),
    ('German Folk Rock', 'German Folk Rock'),
    ('New Wave', 'New Wave')
  )

  related_artist = models.ForeignKey('Artist', on_delete=models.CASCADE)
  album_title = models.CharField(max_length=500)
  release_date = models.DateField()
  genre = models.CharField(max_length=200, choices=GENRE_TYPES)
  date_created = models.DateTimeField(auto_now_add=True)

  def __str__(self):
    return self.album_title + " - " + self.related_artist.artist_name


class Artist(models.Model):
  artist_name = models.CharField(max_length=250)
  latest_release = models.ForeignKey(Album, on_delete=models.SET_NULL, null=True, blank=True)
  date_created = models.DateTimeField(auto_now_add=True)
  
  def __str__(self):
    return self.artist_name
  