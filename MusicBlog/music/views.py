from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from .models import Album, Artist

# Create your views here.

def get_albums() -> list:
  return [
    {
      "title": "Artus",
      "band": "Schandmaul",
      "release": 2019,
      "genre": "German-Folk-Rock"
    },
    {
      "title": "Reprise",
      "band": "Moby",
      "release": 2021,
      "genre": "EDM"
    },
    {
      "title": "Sonne",
      "band": "Rammstein",
      "release": 2001,
      "genre": "NDH"
    },
    {
      "title": "Death Mans Party",
      "band": "Oingo Boingo",
      "release": 1985,
      "genre": "New Wave"
    },
    {
      "title": "How to be a Human Being",
      "band": "Glass Animals",
      "release": 2016,
      "genre": "Indie Rock"
    },
  ]

@csrf_exempt
def home(request):
  if request.method == 'POST':
    related_artist = request.POST.get("related_artist", "Default Artist")
    album_title = request.POST.get("album_title", "Default Title")
    release_date = request.POST.get("release_date", "Default Date")
    genre = request.POST.get("genre", "Default Genre")
    new_album = Album.objects.create(
      related_artist=related_artist,
      album_title=album_title,
      release_date=release_date,
      genre=genre) #.save()
    Album.save()
    # new_album.save()
  else:
    all_albums = Album.objects.all()
    return render(request, 'music/dashboard.html', { "title": "Dashboard", "albums": all_albums})

def about(request):
  return render(request, 'music/about.html', {"title": "About"})

def details(request, pk):
  pk = pk
  album = Album.objects.get(pk=pk)
  title_string = "Details: "
  title_string += album.album_title
  return render(request, 'music/details.html', {"title": title_string, "album": album})
