from django.urls import path

from .views import about, details, home

urlpatterns = [
  path('', home),
  path('about/', about),
  # localhost:8000/music/album/1
  path('album/<int:pk>', details, name='DetailAlbum'),
]
